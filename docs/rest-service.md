## Create REST composite μ-service

1. [Prerequisites](#prerequisites)
1. [REST API definition](#rest-api-definition)
2. [Studio Project setup](#studio-project-setup)
2. [Preparation of Interfaces](#preparation-of-interfaces)
2. [Preparation of REST and SOAP endpoints](#preparation-of-rest-and-soap-endpoints)
2. [Create Route ‘Main’](#create-route-main)
2. [Create Route ‘Call S1’](#create-route-call-s1)
2. [Create Route ‘Call S2’](#create-route-call-s2)
2. [Test locally](#test-locally)
2. [Deploy in OpenShift](#deploy-in-openshift)



## Prerequisites

This tutorial has been built and tested using the following product versions:

- Red Hat CodeReady Studio [12.11.0] GA
- OpenShift [v3.11.82] (Minishift)
- Google Chrome (Mac)
- Swagger UI Chrome App [0.0.7]

This guide will help you to complete the REST services shown in the picture below.

The project requires to have previously built and deployed the SOAP services S1 and S2. Instructions on how to build and deploy the SOAP services have also been provided.

 ![full view](./rest/image59.png)

**<center>Overview of full demo project</center>**

<br/><br/>

---

## REST API Definition

The tutorial follows an API-First approach.\
The contract between client and server is defined first, and then used to auto-generate code to assist with the implementation, compliant with the interface.

The API will be defined with the spec OpenAPI 2.0 using **[Apicurio Studio](https://studio.apicur.io)**.

> **NOTE:** A *'Lite'* version of *Apucurio* can be deployed in OpenShift (Apicurito).

Login to [Apicurio Studio](https://studio.apicur.io) using your credentials of preference from:

    https://studio.apicur.io

Once logged in, if never used the tool before, you should see the following from the *Dashboard*.

![](./rest/image61.png)


Click **'Create New API'** and name it **'demoapi'** specifying its type as **'Open API 2.0 (Swagger)'**

![](rest/image62.png)

Click **'Create API'**, and then **'Edit API'**

You are now in the API definition section.\
Add a new data type:

![](rest/image63.png)

Name the type `DemoData`, and add the following sample

```json
{
    "data1": "dummy",
    "data2": "dummy",
    "data3": "dummy",
    "data4": "dummy"
}
```

Click **Save**.

Now the operations need to be defined, click on **'Add a path'**

![](rest/image64.png)

and define the path as:

    /data

Once created, you'll find no operations are yet defined.

Let's create the `POST` operation.

> **NOTE:** for simplicity the request and response types will be the same

Select `Post` from the available operation tabs and click the button **'Add Operation'**  performing the following actions:

- add an Operation ID as `createData` and click the **'check'** button
- add `application/json` in **Consumes(Inputs)**  and click the **'check'** button
- add `application/json` in **Produces(Outputs)**  and click the **'check'** button

The definition should look like:

![](rest/image65.png)

In the `REQUEST BODY` section enter a description and the `DemoData` as body type:

![](rest/image67.png)

On the **RESPONSES** section, perform the following actions:

- add a `200 OK` status code
- add the `submits data` description and click the **'check'** button
- add the `DemoData` type

The definition should look like:

![](rest/image68.png)

The REST API definition is now completed.

<br/>

---

## Studio Project setup

Create a new Fuse project from Studio.


 ![full view](./soap/image13.png)

Project Name ​ **“rest”**

Click ‘Next’ twice: \
Next > \
Next >

 ![full view](./soap/image25.png)

Click **‘Finish’**.\
This action will auto-generate the project skeleton.


Delete default route:

![](./rest/image42.png)

Open the POM file\
(Project’s definition)

Update the Maven coordinates\
(for convenience)

```xml
<groupId>org.demo</groupId>
<artifactId>rest</artifactId>
<version>1.0.0</version>
```
Include the following starter dependencies
```xml
<dependency>
    <groupId>org.apache.camel</groupId>
    <artifactId>camel-servlet-starter</artifactId>
</dependency>
<dependency>
    <groupId>org.apache.camel</groupId>
    <artifactId>camel-jackson-starter</artifactId>
</dependency>
<dependency>
    <groupId>org.apache.camel</groupId>
    <artifactId>camel-swagger-java-starter</artifactId>
</dependency>
<dependency>
    <groupId>org.apache.camel</groupId>
    <artifactId>camel-jaxb-starter</artifactId>
</dependency>

<!-- OpenAPI dependencies -->
<dependency>
    <groupId>com.google.code.gson</groupId>
    <artifactId>gson</artifactId>
    <version>2.8.5</version>
</dependency>
<dependency>
    <groupId>org.threeten</groupId>
    <artifactId>threetenbp</artifactId>
    <version>1.3.7</version>
</dependency>
```

## Preparation of Interfaces

First, include the REST contract created with Apicurio Studio.\
Under the following source folder:

    src/main/resources

create a new folder ​ `openapi`

![](./rest/image18.png)

Create a new file and name it `demodata.json`.
Now, from Apicurio Studio, click the button **'Live Documentation'** at the top right corner.\
A new browser tab opens, click **'Download'**:

![](./rest/image69.png)

Now copy & paste the OpenAPI definition into the newly created JSON file in Studio and save it. You should now have the interface included into the project:

![](./rest/image70.png)







---

Now, include the backend S1 and S2 interfaces.\
Create a new folder ​ **‘wsdl’**

![](./rest/image18.png)

Import (copy) the 2 WSDLs from S1 and S2 services. You should see:

![](./rest/image39.png)

---
Add POM plugins:\
(these plugins auto-generate the necessary sources and classes from the interfaces above included.)

```xml
<!-- REST plugin -->
<plugin>
    <groupId>org.apache.camel</groupId>
    <artifactId>camel-restdsl-swagger-plugin</artifactId>
    <version>2.24.0</version>
    <configuration>
        <specificationUri>src/main/resources/openapi/demodata.json</specificationUri>
        <fileName>camel-rest.xml</fileName>
        <outputDirectory>src/main/resources/spring</outputDirectory>
        <modelOutput>${basedir}/target/generated</modelOutput>
    </configuration>
    <executions>
        <execution>
        <id>generate-sources-rest</id>
        <phase>generate-sources</phase>
        <?m2e execute?>
        <goals>
            <goal>generate-xml-with-dto</goal>
        </goals>
        </execution>
</executions>
</plugin>

<!-- SOAP plugin -->
<plugin>
    <groupId>org.apache.cxf</groupId>
    <artifactId>cxf-codegen-plugin</artifactId>
    <executions>
        <execution>
        <id>generate-sources-soap</id>
        <phase>generate-sources</phase>
        <goals>
            <goal>wsdl2java</goal>
        </goals>
        <configuration>
            <sourceRoot>${basedir}/target/generated/src/main/java</sourceRoot>
            <wsdlRoot>${basedir}/src/main/resources/wsdl</wsdlRoot>
        </configuration>
        </execution>
    </executions>
</plugin>
```

With the POM file open click on the play button in the tool bar:

![](./rest/image71.png)


and select `'Maven generate-sources'`:

![](./rest/image72.png)

The above action should ensure the following sources are gerenareted:

- WSDL Java Objects
- REST Java Objects
- REST DSL operations

## Preparation of REST and SOAP endpoints

Open​ **‘Camel-Context.xml’** ​ in Source view

![](./rest/image55.png)

a) Add the namespace:

    xmlns:cxf="http://camel.apache.org/schema/cxf"

with `‘xsi:schemaLocation’` set to

    http://camel.apache.org/schema/cxf http://camel.apache.org/schema/cxf/camel-cxf.xsd

b) and the endpoints for both services S1 and S2 in the Bean part (outside the Camel
context part):

```xml
<cxf:cxfEndpoint
    id="cxfS1"
    address="/s1"
    serviceClass="org.example.s1.S1"/>

<cxf:cxfEndpoint
    id="cxfS2"
    address="/s2"
    serviceClass="org.example.s2.S2"/>
```

Should look like this:

![](./rest/image49.png)

Let’s also prepare the dummy requests for S1 and S2 by defining Beans as follows:\
(the classes were already autogenerated earlier)

```xml
<bean class="org.example.s1.S1Request" id="dummyS1">
  <property name="param1" value="dummy1 S1"/>
  <property name="param2" value="dummy2 S1"/>
</bean>
<bean class="org.example.s2.S2Request" id="dummyS2">
  <property name="param1" value="dummy1 S2"/>
  <property name="param2" value="dummy2 S2"/>
</bean>
```
Inside the **CamelContext**, include the following REST configuration
```xml
<restConfiguration apiContextPath="api-docs" component="servlet" contextPath="camel" port="8080" bindingMode="json">
    <apiProperty key="cors" value="true"/>
    <apiProperty key="api.title" value="My First Camel REST API"/>
    <apiProperty key="api.version" value="1.0.0"/>
</restConfiguration>
```

and copy from the **`<rest>`** node, that includes the `POST` operation, auto-generated in the Camel REST definition under:

    src/main/resources/camel-rest.xml

Finally, specify the input type of the `POST` operation by adding the following attribute:

```xml
<post type="io.swagger.client.model.DemoData"
```

it references the Java Object auto-generated from the OpenAPI spec.

The overall REST defitions should look like:

![](./rest/image73.png)

The XML resource `camel-rest.xml` can be deleted at this point.

---

## Create Route ‘Main’

Change to​ **‘Design’** ​ view

![](./rest/image25.png)

Create new Route (type in search bar) and drag and drop ‘Route’ to the drawing pane.

![](./rest/image5.png)

Give it the id​ `Main`

![](./rest/image9.png)

Create a​ **‘direct’** ​component

![](./rest/image27.png)

Under the ​ **‘Advanced’** ​ tab, give the the name `createData`

![](./rest/image37.png)

We add a ​ **‘Log’** ​activity to trace received requests:

![](./rest/image28.png)\
`"REST service call received ${body}"`

Drag and drop a ​ **‘direct’** ​ activity to invoke a sub-route `call-s1`

![](./rest/image5.png)

![](./rest/image36.png)


And another one to call the sub-route `call-s2`

![](./rest/image34.png)

Let’s add now a ​ **‘Data Transformation’** ​ activity. Type ‘data’ in the palette’s search bar:

![](./rest/image53.png)

Select a Java to Java transformation (as we’re using JAXB to handle our data):

![](./rest/image44.png)

We select ​ **‘S2Response’** ​as the source data (in the body after calling S2).

![](./rest/image35.png)

![](./rest/image13.png)


We select ​ **‘DemoData’** ​ as the destination data (intended data to send back)

![](./rest/image38.png)

Click​ **'OK'** then **‘Finish’** ​.

On the ‘Transformation’ window, click the icon ​ **‘Add a new mapping’** ​, as shown below:

![](./rest/image56.png)

Then drag and drop and the right side the response field ​ **‘data1’** ​:

![](./rest/image21.png)

For the left part, click the small arrow indicator and select ​ **‘Set expression’**

![](./rest/image1.png)

Enter the `Simple` language and the expression:\
> `${property.s1response.return1}`

![](./rest/image15.png)

Please note the use of the `Simple` language to access the​ **`‘s1response’`** ​body from a property to be defined later after calling service S1.

Now add another ‘Expression’ mapping:

> `${property.s1response.return2}` → `data2`

And finally map from S2Response the 2 returned fields

>`return1` → `data3`\
>`return2` → `data4`

The result transformation should look as follows:

![](./rest/image22.png)

Include a **'Remove Headers'** activity with the wildcard `*` as the matching pattern to remove all headers. This avoids header conflicts between different endpoint interactions.

![](./rest/image76.png)\
![](./rest/image77.png)

And the resulting route should look like:

![](./rest/image50.png)

---

## Create Route ‘Call S1’

Create new Route (type in search bar) and drag and drop ‘Route’ to the drawing pane.

![](./rest/image5.png)

Give it the id​ **‘Call S1’**

![](./rest/image74.png)

Create a​ **‘direct’** ​ component and call it ​ `call-s1`

![](./rest/image27.png)

![](./rest/image23.png)

Include a **'Remove Headers'** activity with the wildcard `*` as the matching pattern to remove all headers. This avoids header conflicts between different endpoint interactions.

![](./rest/image76.png)\
![](./rest/image77.png)



Include a​ **‘setBody’** ​ activity to inject the Bean with the dummy request: `ref:dummyS1`

![](./rest/image47.png)

>Note the use of prefix ​ **‘ref’** ​ to refer to the declared bean ​ **‘dummyS1’** ​.

Set the header `CamelDestinationOverrideUrl` to define the address of the endpoint.

![](./rest/image26.png)

> **NOTE:** the HTTP address is pointing to the S1 in OpenShift

Choose from the palette the CXF starting activity:
Define it as:

![](./rest/image19.png)


Where `cxfS1` references our previously CXF S1 defined Endpoint.

![](./rest/image40.png)


We add a​ **‘Log’** ​ activity to display the response obtained from S1:

![](./rest/image28.png)\
`got response from S1 ${body[0]}`

>Note how we’re using the index-0, CXF provides the response using a list where the response is
stored in index 0.

We add a step to keep in a property the response obtained from invoking the CXF endpoint.\
Drag and drop a ​ **‘Set Property’** ​activity

![](./rest/image52.png)

set the property `s1response` ​to `body[0]`, as shown below

![](./rest/image54.png)

> **NOTE:** we’re storing index 0, as CXF uses a list to provide the response, where the response is
stored on index 0.

The final route should look as follows:

![](./rest/image11.png)


## Create Route ‘Call S2’

Create a new Route (type in search bar) and drag and drop ‘Route’ to the drawing pane.

![](./rest/image5.png)

Give it the id ​ **‘Call S2’**

![](./rest/image75.png)

Create a ​ **‘direct’** ​ component and call it​ `call-s2`

![](./rest/image27.png)

![](./rest/image32.png)

Include a **'Remove Headers'** activity with the wildcard `*` as the matching pattern to remove all headers. This avoids header conflicts between different endpoint interactions.

![](./rest/image76.png)\
![](./rest/image77.png)

Include a​ **‘setBody’** ​ activity to inject the Bean with the dummy request: `ref:dummyS2`

![](./rest/image12.png)

> Note the use of prefix ​ **‘ref’** ​ to refer to the declared bean ​ **‘dummyS2’** ​.

Set the header `CamelDestinationOverrideUrl` to establish the address of the endpoint.

![](./rest/image45.png)

Choose from the palette the CXF starting activity:

![](./rest/image19.png)

Where `cxfS2` references our previously CXF S2 defined Endpoint.

![](./rest/image57.png)

We add a ​ **‘Log’** ​activity to display the response obtained from S2:

![](./rest/image28.png)\
`got response from S2 ${body[0]}`

> **NOTE:** how we’re using the index-0, CXF provides the response using a list where the response is
stored in index 0.

The final route should look as follows:

![](./rest/image14.png)

<br/>

- - - -

## Test locally

> **NOTE:** the REST application will run locally, but the endpoints were configured targetting S1 & S2 deployed on OpenShift.

Run the application by clicking on the 'play' button:

![](./rest/image41.png)

The *'Run as'* dialog will open, choose *'Local Camel Context'*

![](./rest/image7.png)

Click ​ **‘Ok’**

Eclipse should launch a SpringBoot application successfully.\
(if not, you’ll have to fix the problems)

Using Chrome’s Swagger extension, you can visualise the REST services available:\
Explore URL: `http://localhost:8080/camel/api-docs`

![](./rest/image17.png)

![](./rest/image33.png)

Click on the service to unfold it, and click in the input model to fill in the sample request:

![](./rest/image43.png)

And hit '**try it out!**’

You should obtain the following response:

![](./rest/image30.png)



<br/>

**<center>You’ve successfully completed the REST μ-service !!</center>**

<br/><br/><br/>

- - - -


## Deploy in OpenShift

(in this example, ‘minishift’)

> **_NOTE:_**  Fuse 7.3 OpenShift images need to be installed.

As the services S1 and S2 were previously deployed, the demo project should already be visible in the ‘OpenShift Explorer’ view:

![](./rest/image51.png)
 
Now from the Project’s root, right click and select Run As -> Run Configurations...

![](./rest/image2.png)

And ensure the following JRE parameters are properly set:

```properties
-Dkubernetes.master=https://192.168.99.100:8443
-Dkubernetes.namespace=demo
-Dkubernetes.auth.basic.username=developer
-Dkubernetes.auth.basic.password=developer
```

Click ​ **‘Run’** ​ and JBDS should trigger the deployment in OpenShift.

> **TIP:** if the deployment fails, there might be an environmental issue on your machine. Add the flag `-X` in the Maven goal ('Main' tab) and run again the deployment:\
`-X clean install fabric8:deploy`\
![full view](./soap/image38.png)\
Inspect the output logs for traces of possible causes for error.

Once deployed, you should be able from the console to visualise the pod running.

![](./rest/image48.png)

An OCP route should also have been auto-created to accept external traffic.

The OpenAPI definition (Swagger) should be accessible from the following URL address:

    http://rest-demo.192.168.99.100.nip.io/camel/api-docs


From a browser you should obtain the following:

![](./rest/image10.png)

However we will setup a port-forwad with the ​ **‘oc’** ​client, as the Swagger definition uses a local address by default.

Open a terminal and execute the following command:

    oc get pods


This will return something like:

![](./rest/image16.png)

Use the pod name to invoke the port-forward command as follows:

    oc port-forward rest-1-1kg67 8080

You should see the following output:

![](./rest/image6.png)


Now, just like you tested locally, using Chrome’s Swagger extension, you can visualise the
published REST service:
Explore URL: `http://localhost:8080/camel/api-docs`

![](./rest/image17.png)

![](./rest/image33.png)

Click on the service to unfold it, and click in the input model to fill in the sample request:

![](./rest/image43.png)

And hit '**try it out!**'

You should obtain the following response:

![](./rest/image30.png)

<br/>

**<center>You’ve successfully deployed the REST service in
OpenShift !!</center>**

<br/><br/><br/>
