# REST composite service tutorial (using CodeReady Studio)

This tutorial will guide you on how to build an API-First REST service integrated with two different SOAP backend services (also included). The data from the SOAP responses is combined with a data mapper and returned to the REST client. The tutorial also guides you on how to deploy the full solution in OpenShift.

The REST service is implemented using Camel's REST DSL, and all the SOAP interactions at the back are implemented using Camel's CXF component. All three systems are SpringBoot based.

The picture below shows all the components the tutorial will help you to build step by step:

![Overview of the full solution](images/fullview.png)

The instructions are provided in the links below.
1. [Instructions for SOAP services](docs/soap-service.md)\
Guide on how to build the backend SOAP services. This one set of instructions is valid for both SOAP services S1 & S2.
2. [Instructions for REST service](docs/rest-service.md)\
Guide on how to build the frontend REST composite service.



